//
//  CityForecastDetailViewController.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/6/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "UICityForecastDetailViewController.h"

@implementation UICityForecastDetailViewController

MultiForecast *forecast;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fillUIElements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)fillUIElements{
    _city.text = forecast.cityAndState;
    _currentTime.text = [forecast.date formatDateToVerboseString];
    _currentCondition.text = [forecast.forecasts[0] formatWeatherConditionToString];
    _highTemperature.text = [NSString stringWithFormat:@"%i", forecast.forecasts[0].highTemperature];
    _lowTemperature.text = [NSString stringWithFormat:@"/ %i°", forecast.forecasts[0].lowTemperature];
    _currentHumidity.text = [NSString stringWithFormat:@"%i%@", [forecast.date getStartingDayNumber], @"%"];
    
    NSString* conditionIcon = [NSString stringWithFormat:@"BLK-%@", [forecast.forecasts[0] getWeatherConditionIconName:false]];
    _currentConditionIcon.image = [UIImage imageNamed:conditionIcon];
    
    self.navigationItem.title = [forecast.date formatDayToString];

//    [_day2 fillUIElementsWithForecast:forecast dayIndex:1];
//    [_day3 fillUIElementsWithForecast:forecast dayIndex:2];
//    [_day4 fillUIElementsWithForecast:forecast dayIndex:3];
//    [_day5 fillUIElementsWithForecast:forecast dayIndex:4];
}

- (void)setForecast:(MultiForecast *)mforecast{
    forecast = mforecast;
}

#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}

@end
