//
//  Forecast.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "WeatherCondition.h"
#import <Foundation/Foundation.h>

@interface Forecast : NSObject

@property int               lowTemperature;
@property int               highTemperature;
@property int               currentHumidity;
@property WeatherCondition  currentCondition;
@property int               currentTemperature;

// Restrictive Setters
- (void)setHumidity:(int)humidity;
- (void)setLowTemperature:(int)lowTemperature;
- (void)setHighTemperature:(int)highTemperature;

// Calculative Getters
- (NSString*)getWeatherConditionIconName:(Boolean)isNight;
- (NSString*)formatWeatherConditionToString;
+ (WeatherCondition)convertStringToWeatherCondition:(NSString*)value;

// Initializers
- (Forecast*)initWithCurrentCondition:(WeatherCondition)weatherCondition lowTemperature:(int)lowTemperature highTemperature:(int)highTemperature humidity:(int)humidity;
- (Forecast*)initWithCurrentCondition:(WeatherCondition)weatherCondition lowTemperature:(int)lowTemperature highTemperature:(int)highTemperature currentTemperature:(int)currentTemperature humidity:(int)humidity;
@end
