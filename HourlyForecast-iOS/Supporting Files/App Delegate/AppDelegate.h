//
//  AppDelegate.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/5/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

