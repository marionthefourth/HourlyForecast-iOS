//
//  UICityForecastTableViewController.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/6/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "MultiForecast.h"
#import <UIKit/UIKit.h>

@interface UICityForecastTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *searchField;
- (IBAction)tappedSearchButton:(UIBarButtonItem *)sender;
@property NSMutableArray<MultiForecast*>* cityForecasts;

- (void)registerNibs;
- (void)pullDataFromOpenWeatherAPI:(NSString*)city;
@end
