//
//  UISecondaryDayForecastView.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/8/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "MultiForecast.h"
#import <UIKit/UIKit.h>

@interface UISecondaryDayForecastView : UIView

@property IBOutlet UILabel *dayOfWeek;
@property IBOutlet UILabel *highTemperature;
@property IBOutlet UIImageView *currentCondition;

@property int dayIndex;
@property MultiForecast* forecast;

- (void)fillUIElementsWithForecast:(MultiForecast*)forecast dayIndex:(int)dayIndex;

+ (NSString*)reuseIdentifier;
+ (NSString*)nibName;

@end
