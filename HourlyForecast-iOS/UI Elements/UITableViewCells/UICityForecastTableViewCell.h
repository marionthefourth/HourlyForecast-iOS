//
//  UICityForecastCellTableViewCell.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/6/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "MultiForecast.h"
#import <UIKit/UIKit.h>

@interface UICityForecastTableViewCell : UITableViewCell

@property IBOutlet UILabel *cellCity;
@property IBOutlet UILabel *lowTemperature;
@property IBOutlet UILabel *highTemperature;
@property IBOutlet UILabel *currentCondition;
@property IBOutlet UIImageView *currentConditionIcon;
@property IBOutlet UILabel *currentShorthandDayAndTime;


@property MultiForecast* forecast;

- (void)fillUIElementsWithForecast:(MultiForecast*)forecast;
+ (NSString*)reuseIdentifier;
+ (NSString*)nibName;
@end
