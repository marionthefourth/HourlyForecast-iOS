//
//  main.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/5/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
