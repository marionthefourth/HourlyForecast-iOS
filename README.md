# HourlyForecast-iOS
This application uses OpenWeatherMap API to pull a 5 day/3 hour forecast to display weather forecasts for a given city. Users can search forecasts by cities via a text input. Users can also tap on an individual list item and navigate to a view that provides additional details about the forecast.
