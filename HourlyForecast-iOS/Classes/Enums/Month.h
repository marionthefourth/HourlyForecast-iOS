//
//  Month.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

@interface EMonth

typedef enum{
    MONTH_JANUARY     =0,
    MONTH_FEBUARY     =1,
    MONTH_MARCH       =2,
    MONTH_APRIL       =3,
    MONTH_MAY         =4,
    MONTH_JUNE        =5,
    MONTH_JULY        =6,
    MONTH_AUGUST      =7,
    MONTH_SEPTEMBER   =8,
    MONTH_OCTOBER     =9,
    MONTH_NOVEMBER    =10,
    MONTH_DECEMBER    =11
} Month;

@end

#ifndef Month_h
#define Month_h
#endif /* Month_h */
