//
//  Day.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

@interface EDay

typedef enum{
    DAY_MONDAY    =0,
    DAY_TUESDAY   =1,
    DAY_WEDNESDAY =2,
    DAY_THURSDAY  =3,
    DAY_FRIDAY    =4,
    DAY_SATURDAY  =5,
    DAY_SUNDAY    =6
} Day;

@end

#ifndef Day_h
#define Day_h
#endif /* Day_h */
