//
//  Temperatures.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/9/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Temperatures : NSObject

+ (int)kelvinToCelsius:(double)kelvin;
+ (int)kelvinToFahrenheit:(double)kelvin;
+ (int)fahrenheitToCelsius:(int)fahrenheit;

@end
