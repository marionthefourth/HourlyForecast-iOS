//
//  UICityForecastTableViewController.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/6/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//


#import "Date.h"
#import "Forecast.h"
#import "AFNetworking.h"
#import "MultiForecast.h"
#import "OpenWeatherAPI.h"
#import "UICityForecastDetailViewController.h"
#import "UICityForecastTableViewController.h"
#import "UICityForecastTableViewCell.h"

@implementation UICityForecastTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Register Nib for TableViewCells
    [self registerNibs];
    
    self.navigationItem.title = @"Cities";
    [self pullDataFromOpenWeatherAPI:@"New York"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)tappedSearchButton:(UIBarButtonItem *)sender{

    [self pullDataFromOpenWeatherAPI:[_searchField.text capitalizedString]];
    _searchField.text = @"";
}

- (void)pullDataFromOpenWeatherAPI:(NSString*)city{
    NSString *encoded = [[OpenWeatherAPI apiRef:city] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL = [NSURL URLWithString:encoded];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            [self addForecast:city data:responseObject];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

- (void)addForecast:(NSString*)city data:(NSDictionary*)data{
    NSMutableArray<MultiForecast*>* cityForecastsCopy = [_cityForecasts copy];
    if ([cityForecastsCopy count] > 0) {
        for (int i = 0; i < [cityForecastsCopy count]; i++) {
            if ([cityForecastsCopy[i].cityAndState isEqualToString:city]) {
                // Update That Entry
                [_cityForecasts replaceObjectAtIndex:i withObject:[[MultiForecast alloc] initWithDictionary:data city:city]];
                [self.tableView reloadData];
                return;
            }
        }
    }
    
    [_cityForecasts addObject:[[MultiForecast alloc] initWithDictionary:data city:city]];
    [self.tableView reloadData];
}

#pragma mark - Navigation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"segueToDetailView" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UICityForecastDetailViewController* destinationViewController = segue.destinationViewController;
    
    [destinationViewController setForecast:[_cityForecasts objectAtIndex:[self.tableView indexPathForSelectedRow].row]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cityForecasts count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UICityForecastTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UICityForecastTableViewCell reuseIdentifier] forIndexPath:indexPath];
 
    if (cell == nil) {
        cell = [[UICityForecastTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[UICityForecastTableViewCell reuseIdentifier]];
    }
    
    [cell fillUIElementsWithForecast:[_cityForecasts objectAtIndex:indexPath.row]];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140.f;
}

- (void)registerNibs{
    _cityForecasts = [[NSMutableArray alloc] init];

    [self.tableView registerNib:[UINib nibWithNibName:[UICityForecastTableViewCell nibName] bundle:nil] forCellReuseIdentifier:[UICityForecastTableViewCell reuseIdentifier]];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [_cityForecasts removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    
    [tableView reloadData];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

@end
