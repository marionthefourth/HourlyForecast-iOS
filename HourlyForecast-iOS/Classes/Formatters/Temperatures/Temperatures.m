//
//  Temperaturmes.
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/9/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Temperatures.h"

@implementation Temperatures

+ (int)kelvinToCelsius:(double)kelvin{
    return kelvin -  273.15;
}

+ (int)fahrenheitToCelsius:(int)fahrenheit{
    return (fahrenheit - 32) * ((int)5.0/9.0);
}

+ (int)kelvinToFahrenheit:(double)kelvin{
    return (int)(kelvin * ((int)9.0/5.0) - 459.67);
}

@end
