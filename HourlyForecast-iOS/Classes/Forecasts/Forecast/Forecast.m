//
//  Forecast.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Forecast.h"

@implementation Forecast

- (Forecast*)initWithCurrentCondition:(WeatherCondition)weatherCondition lowTemperature:(int)lowTemperature highTemperature:(int)highTemperature humidity:(int)humidity{
    if (self = [super init]){
        _currentHumidity    = humidity;
        _lowTemperature     = lowTemperature;
        _highTemperature    = highTemperature;
        _currentCondition   = weatherCondition;
    }
    return self;
}

- (Forecast*)initWithCurrentCondition:(WeatherCondition)weatherCondition lowTemperature:(int)lowTemperature highTemperature:(int)highTemperature currentTemperature:(int)currentTemperature humidity:(int)humidity{
    if (self = [self initWithCurrentCondition:weatherCondition lowTemperature:lowTemperature highTemperature:highTemperature humidity:humidity]){
        _currentTemperature = currentTemperature;
    }
    return self;
}

- (void)setHumidity:(int)humidity{
    const int HUMIDITY_MIN = 0;
    const int HUMIDITY_MAX = 100;
    
    if (humidity < HUMIDITY_MIN) {
        _currentHumidity = HUMIDITY_MIN;
    } else if (humidity > HUMIDITY_MAX) {
        _currentHumidity = HUMIDITY_MAX;
    } else {
        _currentHumidity = humidity;
    }
}



- (NSString*)getWeatherConditionIconName:(Boolean)isNight{
    switch(_currentCondition){
        case WEATHERCONDITION_MIST:             return @"Mist";
        case WEATHERCONDITION_RAIN:
            if (isNight) {
                return @"Rain-Night";
            } else {
                return @"Rain-Day";
            }
        case WEATHERCONDITION_SNOW:             return @"Snow";
        case WEATHERCONDITION_CLEAR_SKY:
            if (isNight) {
                return @"Clear-Sky-Night";
            } else {
                return @"Clear-Sky-Day";
            }
        case WEATHERCONDITION_FEW_CLOUDS:
            if (isNight) {
                return @"Few-Clouds-Night";
            } else {
                return @"Few-Clouds-Day";
            }
        case WEATHERCONDITION_SHOWER_RAIN:      return @"Shower-Rain";
        case WEATHERCONDITION_THUNDERSTORM:     return @"Thunderstorm";
        case WEATHERCONDITION_BROKEN_CLOUDS:    return @"Broken-Clouds";
        case WEATHERCONDITION_SCATTERED_CLOUDS: return @"Scattered-Clouds";
    }
    return nil;
}

+ (WeatherCondition)convertStringToWeatherCondition:(NSString*)value{
    if ([value isEqualToString:@"01d"] | [value isEqualToString:@"01n"]){
        return WEATHERCONDITION_CLEAR_SKY;
    } else if ([value isEqualToString:@"02d"] | [value isEqualToString:@"02n"]){
        return WEATHERCONDITION_FEW_CLOUDS;
    } else if ([value isEqualToString:@"03d"] | [value isEqualToString:@"03n"]){
        return WEATHERCONDITION_SCATTERED_CLOUDS;
    } else if ([value isEqualToString:@"04d"] | [value isEqualToString:@"04n"]){
        return WEATHERCONDITION_BROKEN_CLOUDS;
    } else if ([value isEqualToString:@"09d"] | [value isEqualToString:@"09n"]){
        return WEATHERCONDITION_SHOWER_RAIN;
    } else if ([value isEqualToString:@"10d"] | [value isEqualToString:@"10n"]){
        return WEATHERCONDITION_RAIN;
    } else if ([value isEqualToString:@"11d"] | [value isEqualToString:@"11n"]){
        return WEATHERCONDITION_THUNDERSTORM;
    } else if ([value isEqualToString:@"13d"] | [value isEqualToString:@"13n"]){
        return WEATHERCONDITION_SNOW;
    } else if ([value isEqualToString:@"50d"] | [value isEqualToString:@"50n"]){
        return WEATHERCONDITION_MIST;
    }
    
    return WEATHERCONDITION_CLEAR_SKY;
}

- (NSString*)formatWeatherConditionToString{
    switch(_currentCondition){
        case WEATHERCONDITION_MIST:             return @"Misty";
        case WEATHERCONDITION_RAIN:             return @"Rainy";
        case WEATHERCONDITION_SNOW:             return @"Snowing";
        case WEATHERCONDITION_CLEAR_SKY:        return @"Clear Skies";
        case WEATHERCONDITION_FEW_CLOUDS:       return @"Few Clouds";
        case WEATHERCONDITION_SHOWER_RAIN:      return @"Showering Rain";
        case WEATHERCONDITION_THUNDERSTORM:     return @"Thunderstorm";
        case WEATHERCONDITION_BROKEN_CLOUDS:    return @"Broken Clouds";
        case WEATHERCONDITION_SCATTERED_CLOUDS: return @"Scattered Clouds";
    }
    return nil;
}

@end
