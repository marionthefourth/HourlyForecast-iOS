//
//  MultiDate.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "MultiDate.h"

@implementation MultiDate

- (int)getYearAtIndex:(int)index{
    return 2018;
}

- (int)getDayNumberAtIndex:(int)index{
    return 1;
}

- (int)getStartingYear{
    return [self getYear];
}

- (Day)getDayOfWeekAtIndex:(int)index{
    return DAY_MONDAY;
}

- (NSString*)formatDayOfWeekAtIndexToString:(int)index{
    switch([self getDayOfWeekAtIndex:index]) {
        case DAY_MONDAY:    return @"MON";
        case DAY_TUESDAY:   return @"TUE";
        case DAY_WEDNESDAY: return @"WED";
        case DAY_THURSDAY:  return @"THU";
        case DAY_FRIDAY:    return @"FRI";
        case DAY_SATURDAY:  return @"SAT";
        case DAY_SUNDAY:    return @"SUN";
        default: [NSException raise:NSGenericException format:@"Unexpected Day."];
    }
}

- (Month)getMonthOfYearAtIndex:(int)index{
    return MONTH_JANUARY;
}

- (Day)getStartingDayOfWeek{
    return [self getDay];
}

- (Month)getStartingMonthOfYear{
    return [self month];
}

- (int)getStartingDayNumber{
    return [self getDayNumber];
}

- (NSString*)getStartingTime{
    return [self getCurrentTime];
}

- (Boolean)extendsBeyondStartYear{
    return false;
}

- (Boolean)extendsBeyondStartMonth{
    return false;
}

- (void)setStartDate:(Date *)startDate{
    [self setStartingYear:startDate.year];
    [self setStartingDayOfWeek:startDate.day];
    [self setStartingTime:startDate.currentTime];
    [self setStartingMonthOfYearh:startDate.month];
    [self setStartingDayNumber:startDate.getDayNumber];
}

- (void)setStartingTime:(NSString *)startingTime{
    [self setCurrentTime:startingTime];
}

- (void)setStartingYear:(int)startingYear{
    [self setYear:startingYear];
}

- (void)setStartingDayNumber:(int)startingDayNumber{
    [self setDayNumber:startingDayNumber];
}

- (void)setStartingDayOfWeek:(Day)startingDayOfWeek{
    [self setDay:startingDayOfWeek];
}

- (void)setStartingMonthOfYearh:(Month)startingMonthOfYear{
    [self setMonth:startingMonthOfYear];
}

- (MultiDate *)initWithDate:(Date*)date numberOfDays:(int)numberOfDays{
    if (self = [super init]) {
        _numberOfDays = numberOfDays;
        [self setStartDate:date];
    }
    
    return self;
}

@end
