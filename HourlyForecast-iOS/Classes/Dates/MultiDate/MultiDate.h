//
//  MultiDate.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Date.h"
#import <Foundation/Foundation.h>

@interface MultiDate : Date

@property int numberOfDays;

// Standard Getters
- (int)getStartingYear;
- (int)getStartingDayNumber;
- (Day)getStartingDayOfWeek;
- (NSString*)getStartingTime;
- (Month)getStartingMonthOfYear;

// Standard Setters
- (void)setStartDate:(Date*)startDate;
- (void)setStartingYear:(int)startingYear;
- (void)setStartingTime:(NSString*)startingTime;
- (void)setStartingDayNumber:(int)startingDayNumber;
- (void)setStartingDayOfWeek:(Day)startingDayOfWeek;
- (void)setStartingMonthOfYearh:(Month)startingMonthOfYear;

// Calculative Getters
- (int)getYearAtIndex:(int)index;
- (int)getDayNumberAtIndex:(int)index;
- (Day)getDayOfWeekAtIndex:(int)index;
- (Month)getMonthOfYearAtIndex:(int)index;
- (NSString*)formatDayOfWeekAtIndexToString:(int)index;

// Calculative Boolean Values
- (Boolean)extendsBeyondStartYear;
- (Boolean)extendsBeyondStartMonth;

// Initializers
- (MultiDate*)initWithDate:(Date*)date numberOfDays:(int)numberOfDays;

@end
