//
//  OpenWeatherAPI.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/8/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "OpenWeatherAPI.h"

@implementation OpenWeatherAPI

+ (NSString *)apiRef:(NSString *)cityName{
    NSString* URLwithCityQuery = [NSString stringWithFormat:@"https://api.openweathermap.org/data/2.5/weather?q=%@", cityName];
    NSString* appID = [NSString stringWithFormat:@"&appid=%@", [OpenWeatherAPI apiKey]];
    return [NSString stringWithFormat:@"%@%@", URLwithCityQuery, appID];
}

+ (NSString *)apiKey{
    return @"8a629debbbc85ccc3c0a32f967e1db8a";
}
@end
