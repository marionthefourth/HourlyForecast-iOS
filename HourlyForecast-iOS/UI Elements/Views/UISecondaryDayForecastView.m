//
//  UISecondaryDayForecastView.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/8/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "UISecondaryDayForecastView.h"

@implementation UISecondaryDayForecastView

- (void)fillUIElementsWithForecast:(MultiForecast*)forecast dayIndex:(int)dayIndex {
    _forecast = forecast;
    _dayIndex = dayIndex;
    _dayOfWeek.text = [forecast.date formatDayOfWeekAtIndexToString:_dayIndex];
    _highTemperature.text = [NSString stringWithFormat:@"%i", forecast.forecasts[_dayIndex].highTemperature];
    NSString* conditionIcon = [NSString stringWithFormat:@"%@%@", @"WHT-", [forecast.forecasts[_dayIndex] getWeatherConditionIconName:false]];
    _currentCondition.image = [UIImage imageNamed:conditionIcon];
}

+ (NSString*)reuseIdentifier{
    return @"secondarydayforecastview";
}

+ (NSString*)nibName{
    return @"UISecondaryDayForecastView";
}
// Day 2 - FC5830
// Day 3 - FFDA44
// Day 4 - 007AFF
// Day 5 - 27D4C4

@end
