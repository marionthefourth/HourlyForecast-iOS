//
//  UICityForecastDetailViewController.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/6/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultiForecast.h"
#import "UISecondaryDayForecastView.h"

@interface UICityForecastDetailViewController : UIViewController

@property IBOutlet UIImageView *humidityIcon;
@property IBOutlet UIImageView *currentConditionIcon;

@property IBOutlet UILabel *currentTime;
@property IBOutlet UILabel *city;

@property IBOutlet UILabel *lowTemperature;
@property IBOutlet UILabel *currentHumidity;
@property IBOutlet UILabel *highTemperature;
@property IBOutlet UILabel *currentCondition;

// Other Days
@property IBOutlet UISecondaryDayForecastView* day2;
@property IBOutlet UISecondaryDayForecastView* day3;
@property IBOutlet UISecondaryDayForecastView* day4;
@property IBOutlet UISecondaryDayForecastView* day5;

- (void)fillUIElements;
- (void)setForecast:(MultiForecast*)mforecast;

@end
