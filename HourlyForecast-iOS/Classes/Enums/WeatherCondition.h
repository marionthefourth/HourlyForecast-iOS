//
//  WeatherCondition.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

@interface EWeatherCondition

typedef enum{
    WEATHERCONDITION_CLEAR_SKY          =0,
    WEATHERCONDITION_FEW_CLOUDS         =1,
    WEATHERCONDITION_SCATTERED_CLOUDS   =2,
    WEATHERCONDITION_BROKEN_CLOUDS      =3,
    WEATHERCONDITION_SHOWER_RAIN        =4,
    WEATHERCONDITION_RAIN               =5,
    WEATHERCONDITION_THUNDERSTORM       =6,
    WEATHERCONDITION_SNOW               =7,
    WEATHERCONDITION_MIST               =8,
} WeatherCondition;

@end

#ifndef WeatherCondition_h
#define WeatherCondition_h
#endif /* WeatherCondition_h */
