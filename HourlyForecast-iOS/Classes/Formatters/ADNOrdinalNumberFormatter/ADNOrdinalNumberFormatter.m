//
//  ADNOrdinalNumberFormatter.m
//  ADNOrdinalNumberFormatter
//
//  Created by Abizer Nasir
//  https://github.com/Abizern/ADNOrdinalNumberFormatter

/*
 Note from Marion IV,
    I did not create this class, I have cited the original creator of it with his GitHub link, his name and the link is above.
 Purpose: I used this library for the Verbose Date Formatting on the UICityForecastDetailViewController.
 Changes:
        -Scrapped the code that wasn't particular to what I needed as you would typically strap it to a UI Element's natural formatter.
        -Renamed the method to be something simple that gets the point across.
        -Altered parameter type and method type to class
 */

#import "ADNOrdinalNumberFormatter.h"

@implementation ADNOrdinalNumberFormatter

+ (NSString *)formatOrdinal:(int)number {
    NSString *strRepresentation = [NSString stringWithFormat:@"%i", number];
    NSString *lastDigit = [strRepresentation substringFromIndex:([strRepresentation length]-1)];
    
    NSString *ordinal;
    
    if ([strRepresentation hasSuffix:@"11"] || [strRepresentation hasSuffix:@"12"] || [strRepresentation hasSuffix:@"13"]) {
        ordinal = @"th";
    } else if ([lastDigit isEqualToString:@"1"]) {
        ordinal = @"st";
    } else if ([lastDigit isEqualToString:@"2"]) {
        ordinal = @"nd";
    } else if ([lastDigit isEqualToString:@"3"]) {
        ordinal = @"rd";
    } else {
        ordinal = @"th";
    }
    
    return [NSString stringWithFormat:@"%@%@", strRepresentation, ordinal];
}

@end
