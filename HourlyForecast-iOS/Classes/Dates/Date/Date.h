//
//  Date.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Day.h"
#import "Month.h"
#import <Foundation/Foundation.h>

@interface Date : NSObject

// Properties
@property Day day;
@property int year;
@property Month month;
@property int dayNumber;
@property Boolean isNight;
@property NSString *currentTime;

// Calculative Getters
- (int)getNextYear ;
- (Day)getNextDay;
- (Month)getNextMonth;
- (Date*)getTomorrowsDate;
- (NSString*)formatDayToString;
- (NSString*)formatDayToShorthandString;
- (NSString*)formatMonthToString;
- (NSString*)formatMonthToShorthandString;
- (NSString*)formatDateToVerboseString;
- (NSString*)formatDateToShorthandString;

// Standard Getters
- (int)getYear;
- (Day)getDay;
- (Month)getMonth;
- (int)getDayNumber;
- (NSString*)getCurrentTime;

// Restrictive Setters
- (void)setToNextDay;
- (void)setToNextMonth;
- (void)setYear:(int)year;
- (void)setDayNumber:(int)dayNumber;
- (void)setDayWithString:(NSString*)day;

// Modifying Methods
- (void)resetDayNumber;
- (void)incrementDayNumber;
+ (Month)convertNumberToMonth:(NSString*)value;

// Initializers
- (Date*)initWithThisMoment;
- (Date*)initWithFormattedString:(NSString*)dateString;
- (Date*)initWithDay:(Day)day month:(Month)month dayNumber:(int)dayNumber year:(int)year currentTime:(NSString*)currentTime;
@end
