//
//  MultiForecast.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Forecast.h"
#import "Date.h"
#import "MultiDate.h"
#import <Foundation/Foundation.h>

@interface MultiForecast : NSObject

@property MultiDate* date;
@property NSString* cityAndState;
@property NSMutableArray<Forecast*>* forecasts;

- (MultiForecast*)initWithComplexDictionary:(NSDictionary*)dictionary;
- (MultiForecast*)initWithDictionary:(NSDictionary*)dictionary city:(NSString*)city;
- (MultiForecast*)initWithDate:(Date*)date forecasts:(NSMutableArray<Forecast*>*)forecasts cityAndState:(NSString*)cityAndState;

@end
