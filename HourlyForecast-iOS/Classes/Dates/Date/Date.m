//
//  Date.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Date.h"
#import "ADNOrdinalNumberFormatter.h"

@implementation Date


- (NSString*)formatDayToShorthandString{
    switch(_day) {
        case DAY_MONDAY:    return @"MON";
        case DAY_TUESDAY:   return @"TUE";
        case DAY_WEDNESDAY: return @"WED";
        case DAY_THURSDAY:  return @"THU";
        case DAY_FRIDAY:    return @"FRI";
        case DAY_SATURDAY:  return @"SAT";
        case DAY_SUNDAY:    return @"SUN";
        default: [NSException raise:NSGenericException format:@"Unexpected Day."];
    }
};

- (NSString*)formatDayToString{
    switch(_day) {
        case DAY_MONDAY:    return @"Monday";
        case DAY_TUESDAY:   return @"Tuesday";
        case DAY_WEDNESDAY: return @"Wednesday";
        case DAY_THURSDAY:  return @"Thursday";
        case DAY_FRIDAY:    return @"Friday";
        case DAY_SATURDAY:  return @"Saturday";
        case DAY_SUNDAY:    return @"Sunday";
        default: [NSException raise:NSGenericException format:@"Unexpected Day."];
    }
};

- (NSString*)formatMonthToShorthandString{
    switch(_month) {
        case MONTH_JANUARY:     return @"JAN";
        case MONTH_FEBUARY:     return @"FEB";
        case MONTH_MARCH:       return @"MAR";
        case MONTH_APRIL:       return @"APR";
        case MONTH_MAY:         return @"MAY";
        case MONTH_JUNE:        return @"JUN";
        case MONTH_JULY:        return @"JUL";
        case MONTH_AUGUST:      return @"AUG";
        case MONTH_SEPTEMBER:   return @"SEP";
        case MONTH_OCTOBER:     return @"OCT";
        case MONTH_NOVEMBER:    return @"NOV";
        case MONTH_DECEMBER:    return @"DEC";
        default: [NSException raise:NSGenericException format:@"Unexpected Month."];
    }
};

- (NSString*)formatMonthToString{
    switch(_month) {
        case MONTH_JANUARY:     return @"January";
        case MONTH_FEBUARY:     return @"February";
        case MONTH_MARCH:       return @"March";
        case MONTH_APRIL:       return @"April";
        case MONTH_MAY:         return @"May";
        case MONTH_JUNE:        return @"June";
        case MONTH_JULY:        return @"July";
        case MONTH_AUGUST:      return @"August";
        case MONTH_SEPTEMBER:   return @"September";
        case MONTH_OCTOBER:     return @"October";
        case MONTH_NOVEMBER:    return @"November";
        case MONTH_DECEMBER:    return @"December";
        default: [NSException raise:NSGenericException format:@"Unexpected Month."];
    }
};

- (NSString *)formatDateToVerboseString{
    NSString* cardinalDayNumber = [ADNOrdinalNumberFormatter formatOrdinal:_dayNumber];
    return [NSString stringWithFormat:@"%@, %@ %@, %i | %@", [self formatDayToString],[self formatMonthToString], cardinalDayNumber,[self getYear], [self getCurrentTime]];
}

- (Day)getNextDay{
    switch(_day) {
        case DAY_MONDAY:    return DAY_TUESDAY;
        case DAY_TUESDAY:   return DAY_WEDNESDAY;
        case DAY_WEDNESDAY: return DAY_THURSDAY;
        case DAY_THURSDAY:  return DAY_FRIDAY;
        case DAY_FRIDAY:    return DAY_SATURDAY;
        case DAY_SATURDAY:  return DAY_SUNDAY;
        case DAY_SUNDAY:    return DAY_MONDAY;
        default: [NSException raise:NSGenericException format:@"Unexpected Day."];
    }
};

- (NSString*)formatDateToShorthandString{
    return [NSString stringWithFormat:@"%@ | %@", [self formatDayToShorthandString], [self currentTime]];
}

- (Month)getNextMonth{
    switch(_month) {
        case MONTH_JANUARY:     return MONTH_FEBUARY;
        case MONTH_FEBUARY:     return MONTH_MARCH;
        case MONTH_MARCH:       return MONTH_APRIL;
        case MONTH_APRIL:       return MONTH_MAY;
        case MONTH_MAY:         return MONTH_JUNE;
        case MONTH_JUNE:        return MONTH_JULY;
        case MONTH_JULY:        return MONTH_AUGUST;
        case MONTH_AUGUST:      return MONTH_SEPTEMBER;
        case MONTH_SEPTEMBER:   return MONTH_OCTOBER;
        case MONTH_OCTOBER:     return MONTH_NOVEMBER;
        case MONTH_NOVEMBER:    return MONTH_DECEMBER;
        case MONTH_DECEMBER:    return MONTH_JANUARY;
        default: [NSException raise:NSGenericException format:@"Unexpected Month."];
    }
};

- (Date*)getTomorrowsDate{
    Date* tomorrowsDate = self;
    [tomorrowsDate setToNextDay];
    return tomorrowsDate;
}

- (void)setToNextDay{
    if ([self isLastDayInMonth]) {
        [self resetDayNumber];
        [self setToNextMonth];
    } else {
        [self incrementDayNumber];
    }
    
    _day = [self getNextDay];
}

- (void)setToNextMonth {
    if ([self isLastMonthInYear]) {
        _year = [self getNextYear];
        _month = [self getNextMonth];
    }
}

- (void)setDayNumber:(int)dayNumber{
    if (dayNumber < 1) {
        [NSException raise:NSGenericException format:@"Unexpected Value for Day Number."];
    } else if (dayNumber > 31) {
        [NSException raise:NSGenericException format:@"Unexpected Value for Day Number."];
    } else {
        _dayNumber = dayNumber;
    }
}

- (void)setToTomorrow{
    [self setToNextDay];
}

- (int)getYear{
    return _year;
};

- (void)incrementDayNumber{
    _dayNumber += 1;
}

- (void)resetDayNumber{
    _dayNumber = 1;
};

- (int)getNextYear{
    return _year + 1;
};

- (int)getDayNumber{
    return _dayNumber;
};

- (Boolean)isLastDayInMonth{
    switch (_month) {
        case MONTH_JANUARY:
        case MONTH_MARCH:
        case MONTH_MAY:
        case MONTH_JULY:
        case MONTH_AUGUST:
        case MONTH_OCTOBER:
        case MONTH_DECEMBER:
            return _dayNumber == 31;
        case MONTH_APRIL:
        case MONTH_JUNE:
        case MONTH_SEPTEMBER:
        case MONTH_NOVEMBER:
            return _dayNumber == 30;
        case MONTH_FEBUARY:
            return _dayNumber == 28;
        default: [NSException raise:NSGenericException format:@"Unexpected Month."];
    }
    
    return false;
}

- (Boolean)isLastMonthInYear{
    return _month == MONTH_DECEMBER;
}

- (NSString*)getCurrentTime{
    return _currentTime;
};

- (Day)getDay{
    return _day;
};

- (Month)getMonth{
    return _month;
};

- (void)setDayWithString:(NSString *)day{
    if ([day isEqualToString:@"Monday"]) {
        _day = DAY_MONDAY;
    } else if ([day isEqualToString:@"Tuesday"]) {
        _day = DAY_TUESDAY;
    } else if ([day isEqualToString:@"Wednesday"]) {
        _day = DAY_WEDNESDAY;
    } else if ([day isEqualToString:@"Thursday"]) {
        _day = DAY_THURSDAY;
    } else if ([day isEqualToString:@"Friday"]) {
        _day = DAY_FRIDAY;
    } else if ([day isEqualToString:@"Saturday"]) {
        _day = DAY_SATURDAY;
    } else if ([day isEqualToString:@"Sunday"]) {
        _day = DAY_SUNDAY;
    }
}

+ (Month)convertNumberToMonth:(NSString*)value{
    if ([value isEqualToString:@"01"]){
        return MONTH_JANUARY;
    } else if ([value isEqualToString:@"02"]) {
        return MONTH_FEBUARY;
    } else if ([value isEqualToString:@"03"]) {
        return MONTH_MARCH;
    } else if ([value isEqualToString:@"04"]) {
        return MONTH_APRIL;
    } else if ([value isEqualToString:@"05"]) {
        return MONTH_MAY;
    } else if ([value isEqualToString:@"06"]) {
        return MONTH_JUNE;
    } else if ([value isEqualToString:@"07"]) {
        return MONTH_JULY;
    } else if ([value isEqualToString:@"08"]) {
        return MONTH_AUGUST;
    } else if ([value isEqualToString:@"09"]) {
        return MONTH_SEPTEMBER;
    } else if ([value isEqualToString:@"10"]) {
        return MONTH_OCTOBER;
    } else if ([value isEqualToString:@"11"]) {
        return MONTH_NOVEMBER;
    } else if ([value isEqualToString:@"12"]) {
        return MONTH_DECEMBER;
    }
    
    return MONTH_JANUARY;
}

- (Date*)initWithDay:(Day)day month:(Month)month dayNumber:(int)dayNumber year:(int)year currentTime:(NSString*)currentTime {
    if (self = [super init]){
        _day = day;
        _year = year;
        _month = month;
        _dayNumber = dayNumber;
        _currentTime = currentTime;
    }
    return self;
}

- (Date *)initWithFormattedString:(NSString *)dateString{
    if (self = [super init]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"ET"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        
        //now you have the date, you can output the bits you want
        [dateFormatter setDateFormat:@"yyyy"];
        _year = [[dateFormatter stringFromDate:date] intValue];
        
        [dateFormatter setDateFormat:@"MM"];
        _month = [Date convertNumberToMonth:[dateFormatter stringFromDate:date]];
        
        [dateFormatter setDateFormat:@"dd"];
        _dayNumber = [[dateFormatter stringFromDate:date] intValue];
        
        [dateFormatter setDateFormat:@"HH:mm"];
        _currentTime = [dateFormatter stringFromDate:date];
        if ([_currentTime characterAtIndex:0] == '0') {
            _currentTime = [_currentTime substringFromIndex:1];
        }
        
        // Check if time is after noon or not
        if ([_currentTime characterAtIndex:2] == ':' && [_currentTime characterAtIndex:1] != '1') {
            // It is after  11:59 AM
            _currentTime = [NSString stringWithFormat:@"%@ PM", _currentTime];
        } else {
            _currentTime = [NSString stringWithFormat:@"%@ AM", _currentTime];
        }
        
        NSDate *now = [NSDate date];
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"EEEE"];
        [self setDayWithString:[dateFormatter2 stringFromDate:now]];
        
    }
    return self;
}

- (Date *)initWithThisMoment{
    if (self = [super init]){
        NSDate *now = [NSDate date];
    
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"ET"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        //now you have the date, you can output the bits you want
        [dateFormatter setDateFormat:@"yyyy"];
        _year = [[dateFormatter stringFromDate:now] intValue];
        
        [dateFormatter setDateFormat:@"MM"];
        _month = [Date convertNumberToMonth:[dateFormatter stringFromDate:now]];
        
        [dateFormatter setDateFormat:@"dd"];
        _dayNumber = [[dateFormatter stringFromDate:now] intValue];
        
        dateFormatter.dateFormat = @"HH:mm a";
        _currentTime = [dateFormatter stringFromDate:now];
        
        NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
        [dayFormatter setDateFormat:@"EEEE"];
        [self setDayWithString:[dayFormatter stringFromDate:now]];
    }
    return self;
    
}


@end
