//
//  MultiForecast.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/7/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "Date.h"
#import "MultiDate.h"
#import "Temperatures.h"
#import "MultiForecast.h"

@implementation MultiForecast

-(MultiForecast *)initWithComplexDictionary:(NSDictionary *)dictionary{
    if (self = [super init]){
        _forecasts = [[NSMutableArray<Forecast*> alloc] init];
        if ([[dictionary objectForKey:@"list"] isKindOfClass:[NSArray class]]) {
            NSArray* listArray = [dictionary objectForKey:@"list"];
            Date* startingDate;
            Date* nextDate;
            
            int humidity = 0;
            NSString* icon;
            int temperatureCurrent = -1000;
            int temperatureMinimum = -1000;
            int temperatureMaximum = -1000;
            
            for (NSDictionary* data in listArray) {
                if ([[data objectForKey:@"dt_txt"] isKindOfClass:[NSDictionary class]]) {
                    if (!startingDate) {
                        startingDate = [[Date alloc] initWithFormattedString:[data objectForKey:@"dt_txt"]];
                    } else {
                        nextDate = [[Date alloc] initWithFormattedString:[data objectForKey:@"dt_txt"]];
                    }
                }
                
                if (nextDate && startingDate.dayNumber != nextDate.dayNumber) {
                    // Refresh Values after adding Previous Set's Forecast
                    [_forecasts addObject:[[Forecast alloc] initWithCurrentCondition:[Forecast convertStringToWeatherCondition:icon] lowTemperature:temperatureMinimum highTemperature:temperatureMaximum humidity:humidity]];
                    
                    temperatureCurrent = -1000;
                    temperatureMinimum = -1000;
                    temperatureMaximum = -1000;
                }
                
                if ([[data objectForKey:@"main"] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary* main = [data objectForKey:@"main"];
                    humidity = [[main objectForKey:@"humidity"] intValue];
                    if (temperatureMinimum > [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp_min"] doubleValue]]) {
                        temperatureMinimum = [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp_min"] doubleValue]];
                    }
                    if (temperatureMaximum < [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp_max"] doubleValue]]) {
                        temperatureMaximum = [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp_max"] doubleValue]];
                    }
                    if (temperatureCurrent != -1000) {
                        temperatureCurrent = [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp"] doubleValue]];
                    }
                }
                
                if (!icon) {
                    if ([[data objectForKey:@"weather"] isKindOfClass:[NSArray class]]) {
                        NSArray* weather = [data objectForKey:@"weather"];
                        NSDictionary* dWeather = [weather objectAtIndex:0];
                        icon = [dWeather objectForKey:@"icon"];
                    }
                }
                    
                if (startingDate.dayNumber != nextDate.dayNumber) {
                    nextDate = startingDate;
                    nextDate = [[Date alloc] init];
                }
                
            }
        }
    }
    
    return self;
}

- (MultiForecast *)initWithDictionary:(NSDictionary *)dictionary city:(NSString*)city{
    if (self = [super init]){
        NSString * newCity = [city stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        _cityAndState = newCity;
        _forecasts = [[NSMutableArray<Forecast*> alloc] init];
        Date* startingDate;
        
        int humidity;
        NSString* icon;
        int temperatureCurrent = 0;
        int temperatureMinimum;
        int temperatureMaximum;
        
        {
            NSDictionary* main = [dictionary objectForKey:@"main"];
            humidity = [[main objectForKey:@"humidity"] intValue];
            temperatureMinimum = [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp_min"] doubleValue]];
            temperatureMaximum = [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp_max"] doubleValue]];
            temperatureCurrent = [Temperatures kelvinToFahrenheit:[[main objectForKey:@"temp"] doubleValue]];
        }
        
        startingDate = [[Date alloc]initWithThisMoment];
        
        if ([[dictionary objectForKey:@"weather"] isKindOfClass:[NSArray class]]) {
            NSArray* weather = [dictionary objectForKey:@"weather"];
            NSDictionary* dWeather = [weather objectAtIndex:0];
            icon = [dWeather objectForKey:@"icon"];
        }
        
        _date = [[MultiDate alloc] initWithDate:startingDate numberOfDays:1];
        [_forecasts addObject:[[Forecast alloc] initWithCurrentCondition:[Forecast convertStringToWeatherCondition:icon] lowTemperature:temperatureMinimum highTemperature:temperatureMaximum currentTemperature:temperatureCurrent humidity:humidity]];
    }
    return self;
}

- (MultiForecast*)initWithDate:(Date*)date forecasts:(NSMutableArray<Forecast*>*)forecasts cityAndState:(NSString*)cityAndState{
    if (self = [super init]) {
        _date = [[MultiDate alloc] initWithDate:date numberOfDays:(int)[forecasts count]];
        _cityAndState = cityAndState;
        _forecasts = forecasts;
    }
    return self;
}
@end
