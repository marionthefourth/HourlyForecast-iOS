//
//  ADNOrdinalNumberFormatter.h
//  ADNOrdinalNumberFormatter
//
//  Created by Abizer Nasir
//  https://github.com/Abizern/ADNOrdinalNumberFormatter

/*
 Note from Marion IV,
 I did not create this class, I have cited the original creator of it with his GitHub link, his name and the link is above.
 Purpose: I used this library for the Verbose Date Formatting on the UICityForecastDetailViewController.
 Changes:
 -Scrapped the code that wasn't particular to what I needed as you would typically strap it to a UI Element's natural formatter.
 -Renamed the method to be something simple that gets the point across.
 -Altered parameter type and method type to class
 */

#import <Foundation/Foundation.h>

@interface ADNOrdinalNumberFormatter : NSNumberFormatter
+ (NSString*)formatOrdinal:(int)number;
@end
