//
//  UICityForecastCellTableViewCell.m
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/6/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import "UICityForecastTableViewCell.h"


@implementation UICityForecastTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)fillUIElementsWithForecast:(MultiForecast*)forecast {
    _forecast = forecast;
    _cellCity.text = forecast.cityAndState;
    _currentCondition.text = [forecast.forecasts[0] formatWeatherConditionToString];
    _currentShorthandDayAndTime.text = [forecast.date formatDateToShorthandString];
    _highTemperature.text = [NSString stringWithFormat:@"%i", forecast.forecasts[0].highTemperature];
    _lowTemperature.text = [NSString stringWithFormat:@"/ %i°", forecast.forecasts[0].lowTemperature];
    
    NSString* conditionIcon = [NSString stringWithFormat:@"BLK-%@", [forecast.forecasts[0] getWeatherConditionIconName:false]];
    _currentConditionIcon.image = [UIImage imageNamed:conditionIcon];
}

+ (NSString*)reuseIdentifier{
    return @"cityforecastcell";
}

+ (NSString*)nibName{
    return @"UICityForecastTableViewCell";
}

@end
