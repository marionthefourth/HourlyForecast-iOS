//
//  OpenWeatherAPI.h
//  HourlyForecast-iOS
//
//  Created by Marion G. Rucker IV on 4/8/18.
//  Copyright © 2018 Panoramic, Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenWeatherAPI : NSObject

+ (NSString*)apiKey;
+ (NSString*)apiRef:(NSString*)cityName;
@end
